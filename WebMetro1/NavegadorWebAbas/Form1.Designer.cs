﻿namespace NavegadorWebAbas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnRetorna = new System.Windows.Forms.Button();
            this.btnAvanca = new System.Windows.Forms.Button();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.btnIr = new System.Windows.Forms.Button();
            this.btnNovaGuia = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.txtVerificador = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnRetorna
            // 
            this.btnRetorna.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRetorna.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRetorna.Image = ((System.Drawing.Image)(resources.GetObject("btnRetorna.Image")));
            this.btnRetorna.Location = new System.Drawing.Point(56, 43);
            this.btnRetorna.Name = "btnRetorna";
            this.btnRetorna.Size = new System.Drawing.Size(177, 10);
            this.btnRetorna.TabIndex = 0;
            this.btnRetorna.UseVisualStyleBackColor = false;
            this.btnRetorna.Click += new System.EventHandler(this.btnRetorna_Click);
            // 
            // btnAvanca
            // 
            this.btnAvanca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAvanca.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAvanca.Image = ((System.Drawing.Image)(resources.GetObject("btnAvanca.Image")));
            this.btnAvanca.Location = new System.Drawing.Point(327, 43);
            this.btnAvanca.Name = "btnAvanca";
            this.btnAvanca.Size = new System.Drawing.Size(224, 10);
            this.btnAvanca.TabIndex = 0;
            this.btnAvanca.UseVisualStyleBackColor = false;
            this.btnAvanca.Click += new System.EventHandler(this.btnAvanca_Click);
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUrl.Location = new System.Drawing.Point(12, 27);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(1047, 26);
            this.txtUrl.TabIndex = 1;
            // 
            // btnIr
            // 
            this.btnIr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIr.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnIr.Location = new System.Drawing.Point(893, 27);
            this.btnIr.Name = "btnIr";
            this.btnIr.Size = new System.Drawing.Size(78, 18);
            this.btnIr.TabIndex = 0;
            this.btnIr.Text = "IR";
            this.btnIr.UseVisualStyleBackColor = false;
            this.btnIr.Click += new System.EventHandler(this.btnIr_Click);
            // 
            // btnNovaGuia
            // 
            this.btnNovaGuia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovaGuia.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNovaGuia.Location = new System.Drawing.Point(744, 42);
            this.btnNovaGuia.Name = "btnNovaGuia";
            this.btnNovaGuia.Size = new System.Drawing.Size(153, 10);
            this.btnNovaGuia.TabIndex = 0;
            this.btnNovaGuia.Text = "Nova Guia";
            this.btnNovaGuia.UseVisualStyleBackColor = false;
            this.btnNovaGuia.Click += new System.EventHandler(this.btnNovaGuia_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 8);
            this.tabControl1.Location = new System.Drawing.Point(12, 67);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1047, 756);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 2;
            // 
            // txtVerificador
            // 
            this.txtVerificador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVerificador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVerificador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVerificador.Location = new System.Drawing.Point(1150, 34);
            this.txtVerificador.Name = "txtVerificador";
            this.txtVerificador.Size = new System.Drawing.Size(128, 26);
            this.txtVerificador.TabIndex = 3;
            this.txtVerificador.Visible = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(1065, 67);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(411, 756);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1488, 835);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.txtVerificador);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.btnNovaGuia);
            this.Controls.Add(this.btnIr);
            this.Controls.Add(this.btnAvanca);
            this.Controls.Add(this.btnRetorna);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Navegador com Abas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRetorna;
        private System.Windows.Forms.Button btnAvanca;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Button btnIr;
        private System.Windows.Forms.Button btnNovaGuia;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox txtVerificador;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

